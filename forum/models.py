from django.db import models
from django.utils.encoding import smart_unicode

# Create your models here.

class Article(models.Model):
    
    title = models.CharField(max_length =300)
    writer = models.CharField(max_length =120)
    body = models.TextField()
    image = models.FileField(upload_to='Files/%Y/%m/%d',null=True,blank=True)
    private = models.BooleanField(default=False)
    category = models.CharField(max_length =200, db_index=True)
    pinned = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    modified = models.DateTimeField(auto_now_add=False, auto_now=True)
    
    def __unicode__(self):
        
        return smart_unicode(self.title)

    class Meta:
        ordering = ['-pinned','-created']
        

class Article_Comment(models.Model):
    
    comment = models.TextField()
    commentor = models.CharField(max_length =120)
    post_id = models.IntegerField(db_index=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
    def __unicode__(self):
        
        return smart_unicode(self.id)

    class Meta:
        ordering = ['post_id']

class Article_Category(models.Model):

    category = models.CharField(max_length =200, unique=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=True)
    
    def __unicode__(self):
        
        return smart_unicode(self.category)

    class Meta:
        ordering = ['category']