from django.contrib import admin

from models import Article
from models import Article_Comment
from models import Article_Category

class ArticleAdmin(admin.ModelAdmin):
	list_display = ('title','writer','created','private')

admin.site.register(Article,ArticleAdmin)


class Article_CommentAdmin(admin.ModelAdmin):
    list_display = ('comment','commentor','post_id')

admin.site.register(Article_Comment,Article_CommentAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','category')

admin.site.register(Article_Category,CategoryAdmin)  