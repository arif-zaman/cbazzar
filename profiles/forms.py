from django.forms import ModelForm
from models import userProfile

class ProfileForm(ModelForm):

	class Meta:
		model = userProfile
		fields = ['firstname','lastname','username','email','propic']