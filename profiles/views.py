import datetime
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from models import userProfile
from models import employerProfile
from models import siteAbout
from models import siteTerm
from models import siteFAQ
from models import privacyPolicy
from homes.models import allJob
from homes.models import subscribe
from homes.models import application
from homes.models import jobCategory
from homes.models import jobType
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


today = datetime.date.today()


def profile(request,username=""):
	if username == "careerBazzar":
		return HttpResponseRedirect("/careerbazzar/siteadmin/admin_panel/")
	try:
		profile = userProfile.objects.get(userName=username)
		if profile:
			if profile.role == "admin":
				return HttpResponseRedirect("/careerbazzar/siteadmin/admin_panel/")
			else:
				addr = "/careerbazzar/user/" + username + "/"
				return HttpResponseRedirect(addr)
	except:
		try:
			profile = employerProfile.objects.get(cUserName=username)
			if profile:
				if profile.role == "admin":
					return HttpResponseRedirect("/careerbazzar/siteadmin/admin_panel/")
				else:
					addr = "/careerbazzar/employer/" + username + "/"
					return HttpResponseRedirect(addr)
		except:
			HttpResponseRedirect("/careerbazzar/home/")
	
	return HttpResponseRedirect("/careerbazzar/home/")

		
def user_profile(request,username=''):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

		if username==session_name:
			himself = True
	try:
		pro = userProfile.objects.get(userName=username)
		user = User.objects.get(username=username)
	except:
		return HttpResponseRedirect('/careerbazzar/home/')
			
	sub = subscribe.objects.filter(username=username).values_list('categoryName',flat=True).order_by('categoryName')
	job_list = allJob.objects.filter(jobCategory__in =sub,applyDeadline__gte=today,approved=True)
	job_id = application.objects.filter(username=username).values_list('job_id',flat=True).order_by('job_id')
	app = allJob.objects.filter(id__in =job_id)
	job_categories=jobCategory.objects.all().order_by('categoryName')
	
	sub1 = subscribe.objects.filter(username=username).values_list('categoryName',flat=True).order_by('categoryName')
	all_job_categories=jobCategory.objects.values_list('categoryName',flat=True).order_by('categoryName')
	
	a=set(sub1)
	b=set(all_job_categories)

	subscribed=a&b
	unsubscribed=a^b
	



	paginator= Paginator(job_list,10)
	page=request.GET.get('page')

	try:
		job=paginator.page(page)
	except PageNotAnInteger:
		job=paginator.page(1)
	except EmptyPage:
		job =paginator.page(paginator.num_pages)

	#Subcribing catergories
	if request.POST:
		match_subscribed_category=False
		user_subscribes=request.POST.getlist('group[]','')
		
		c=set(user_subscribes)
		d=c^subscribed
		
		for item in d:
			del_subs=subscribe.objects.filter(username=session_name,categoryName=item)
			del_subs.delete()

		if user_subscribes:
			for item in user_subscribes:
				all_subs=subscribe.objects.filter(username__iexact=username,categoryName__iexact=item)
				if all_subs:
					match_subscribed_category=True
				else:
					add_usr_subs=subscribe(username=session_name,categoryName=item)
					add_usr_subs.save()

	return render_to_response("uprofile.html", locals(), context_instance=RequestContext(request))



def edit_uprofile(request,username=''):

	addr = "/careerbazzar/profile/"+username+"/"

	if 'name' in request.session:
		name = request.session['name']

		if request.POST:
			user = userProfile.objects.get(userName=name)
			user.firstName = request.POST.get("firstname", "")
			user.lastName = request.POST.get("lastname", "")
			user.email = request.POST.get("email", "")
			user.contactNo = request.POST.get("contact", "")
			user.gender = request.POST.get("gender", "")
			user.experienceLevel = request.POST.get("experience", "")
			user.eduInstitute = request.POST.get("edu", "")
			user.degree = request.POST.get("degree", "")
			user.certification = request.POST.get("certificate", "")
			user.keySkills = request.POST.get("skill", "")
			user.description = request.POST.get("description", "")
			if request.FILES.get("propic"):
				user.propic = request.FILES.get("propic")
			user.save()

	return HttpResponseRedirect(addr)


def employer_profile(request,username=''):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

		if username==session_name:
			himself = True

	try:
		pro = employerProfile.objects.get(cUserName=username)
		user = User.objects.get(username=username)
		job = allJob.objects.filter(companyName=pro.companyName).order_by('-jobCreated')
		typename = jobType.objects.exclude(typeName=pro.companyCategory)
	except:
		return HttpResponseRedirect('/careerbazzar/home/')
			
	return render_to_response("eprofile.html", locals(), context_instance=RequestContext(request))
	

def edit_eprofile(request,username=''):

	addr = "/careerbazzar/profile/"+username+"/"

	if 'name' in request.session:
		name = request.session['name']

		if username != name:
			return HttpResponseRedirect(addr)

		if request.POST:
			user = employerProfile.objects.get(cUserName=name)
			user.companyName = request.POST.get("cname", "")
			user.companyWebsite = request.POST.get("website", "")
			user.cUserName = request.POST.get("username", "")
			user.companyEmail = request.POST.get("email", "")
			user.companyAddress = request.POST.get("address", "")
			user.contactNo = request.POST.get("contact", "")
			user.companyFax = request.POST.get("fax", "")
			user.companyCategory = request.POST.get("category", "")
			user.description = request.POST.get("description", "")
			if request.FILES.get("logo"):
				user.companyLogo = request.FILES.get("logo")
			user.save()

	return HttpResponseRedirect(addr)


def view_applicants(request,username='',job_id=1):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

		if username!=session_name:
			addr = '/careerbazzar/profile/' + username + '/'
			return HttpResponseRedirect(addr)

		try:
			pro = employerProfile.objects.get(cUserName=username)
			user = User.objects.get(username=username)
			job = allJob.objects.get(id=job_id)
			app = application.objects.filter(job_id=job_id).order_by('created')
		except:
			return HttpResponseRedirect('/careerbazzar/home/')
			
		return render_to_response("view_applicants.html", locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/careerbazzar/home/')


def about(request):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

	about = siteAbout.objects.all()
	return render_to_response('about.html', locals())


def terms(request):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

	term = siteTerm.objects.all()
	return render_to_response('terms.html', locals())


def privacy(request):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

	policy = privacyPolicy.objects.all()
	return render_to_response('privacy.html', locals())


def faq(request):
	if 'name' in request.session:
		session_name = request.session['name']
		login = True

	faq = siteFAQ.objects.all()
	return render_to_response('faq.html', locals())


def change_password(request):
	if request.POST:
		session_name = request.session['name']
		password1 = request.POST.get("password1","")
		password2 = request.POST.get("password2","")
		password3 = request.POST.get("password3","")
		user = User.objects.get(username=session_name)

		if user.check_password(password1):
			if password2==password3:
				try:
					user.set_password(password2)
					user.save()
					success = True
				except:
					alert1 = True
			else:
				alert2 = True
		else :
			alert1 = True

	if 'name' in request.session:
		session_name = request.session['name']
		login = True
		
		
		return render_to_response("chpasswd.html", locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/careerbazzar/login/')


def delete_account(request):
	if request.POST:
		session_name = request.session['name']
		password = request.POST.get("password1","")
		user = User.objects.get(username=session_name)

		if user.check_password(password):
			try:
				user.delete()
				application.objects.filter(username=session_name).delete()
				subscribe.objects.filter(username=session_name).delete()
				return HttpResponseRedirect("/careerbazzar/logout/")
			except:
				alert = True
		else :
			alert = True

	if 'name' in request.session:
		session_name = request.session['name']
		login = True
		
		
		return render_to_response("delete_account.html", locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/careerbazzar/login/')