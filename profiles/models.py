from django.db import models

class userProfile(models.Model):
	firstName = models.CharField(max_length=60)
	lastName = models.CharField(max_length=60)
	userName = models.CharField(max_length=30,unique=True)
	email = models.EmailField(max_length=254,unique=True)
	contactNo = models.CharField(max_length=15,null=True)
	gender = models.CharField(max_length=10,null=True)
	experienceLevel = models.CharField(max_length=60,null=True)
	eduInstitute = models.CharField(max_length=120,null=True,blank=True)
	degree = models.CharField(max_length=120,null=True,blank=True)
	certification = models.TextField(null=True,blank=True)
	keySkills = models.TextField(null=True,blank=True)
	description = models.TextField(null=True,blank=True)
	role = models.CharField(max_length=10,default="user")
	propic = models.FileField(upload_to='Photo/%Y/%m/%d',null=True,blank=True)
	profileCreated = models.DateTimeField(auto_now_add=True,auto_now=False)
	profileModified = models.DateTimeField(auto_now_add=False,auto_now=True)

	class Meta:
		ordering = ['userName']

class employerProfile(models.Model):
	companyName = models.CharField(max_length=160,unique=True,null=False)
	companyWebsite = models.CharField(max_length=160,null=True,blank=True)
	cUserName = models.CharField(max_length=160,unique=True,null=False)
	companyEmail = models.EmailField(max_length=254)
	companyLogo = models.FileField(upload_to='Files/%Y/%m/%d',null=True,blank=True)
	companyAddress = models.TextField()
	contactNo = models.CharField(max_length=15)
	companyFax = models.CharField(max_length=20,null=True,blank=True)
	companyCategory = models.CharField(max_length=60)
	role = models.CharField(max_length=10,default="employer")
	description = models.TextField(null=True,blank=True)
	profileCreated = models.DateTimeField(auto_now_add=True,auto_now=False)
	profileModified = models.DateTimeField(auto_now_add=False,auto_now=True)

	class Meta:
		ordering = ['companyName']

class siteAbout(models.Model):
	about = models.TextField()
	created = models.DateTimeField(auto_now_add=True,auto_now=False)
	edited = models.DateTimeField(auto_now_add=False,auto_now=True)

class siteTerm(models.Model):
	termsofService = models.TextField()
	created = models.DateTimeField(auto_now_add=True,auto_now=False)
	edited = models.DateTimeField(auto_now_add=False,auto_now=True)

class privacyPolicy(models.Model):
	privacyPolicy = models.TextField()
	created = models.DateTimeField(auto_now_add=True,auto_now=False)
	edited = models.DateTimeField(auto_now_add=False,auto_now=True)

class siteFAQ(models.Model):
	question = models.TextField(unique=True)
	answer = models.TextField()
	created = models.DateTimeField(auto_now_add=True,auto_now=False)
	edited = models.DateTimeField(auto_now_add=False,auto_now=True)

	class Meta:
		ordering = ['id']