from django.contrib import admin

from models import allJob
from models import jobCategory
from models import jobType
from models import jobSalary
from models import jobLocation
from models import jobRole
from models import subscribe
from models import application
from models import ads_left_block
from models import ads_right_block
from models import ads_bottom_block

class allJobAdmin(admin.ModelAdmin):
	list_display = ('companyName','jobTitle','jobType')

admin.site.register(allJob,allJobAdmin)



class jobCategoryAdmin(admin.ModelAdmin):
	list_display = ('categoryName',)

admin.site.register(jobCategory,jobCategoryAdmin)


class jobTypeAdmin(admin.ModelAdmin):
	list_display = ('typeName',)

admin.site.register(jobType,jobTypeAdmin)


class jobSalaryAdmin(admin.ModelAdmin):
	list_display = ('salaryRange',)

admin.site.register(jobSalary,jobSalaryAdmin)


class jobLocationAdmin(admin.ModelAdmin):
	list_display = ('locationName',)

admin.site.register(jobLocation,jobLocationAdmin)


class jobRoleAdmin(admin.ModelAdmin):
	list_display = ('roleName',)

admin.site.register(jobRole,jobRoleAdmin)


class subscribeAdmin(admin.ModelAdmin):
	list_display = ('username','categoryName')

admin.site.register(subscribe,subscribeAdmin)


class applicationAdmin(admin.ModelAdmin):
	list_display = ('username','job_id')

admin.site.register(application,applicationAdmin)


class ads_left_blockAdmin(admin.ModelAdmin):
	list_display=('id','ads1','ads2','ads3','ads4')
admin.site.register(ads_left_block,ads_left_blockAdmin)


class ads_right_blockAdmin(admin.ModelAdmin):
	list_display=('id','ads1','ads2','ads3','ads4')
admin.site.register(ads_right_block,ads_right_blockAdmin)

class ads_bottom_blockAdmin(admin.ModelAdmin):
	list_display=('id','ads1','ads2','ads3','ads4')
admin.site.register(ads_bottom_block,ads_bottom_blockAdmin)