from django.shortcuts import render_to_response
from django.shortcuts import HttpResponseRedirect
from django.template import RequestContext
from forms import captchaForm
from django.contrib import auth
from django.contrib.auth.hashers import make_password
from django.core.context_processors import csrf
from django.contrib.auth.models import User
from homes.models import jobCategory
from homes.models import jobType
from profiles.models import userProfile
from profiles.models import employerProfile


def login(request):
    if 'name' in request.session:
        return HttpResponseRedirect("/careerbazzar/home/")
    
    email = request.POST.get("email", "")
    password = request.POST.get("password", "")

    try:
    	user = User.objects.get(email=email)
    	if user.check_password(password):
    		request.session['name'] = user.username
    		if request.session['name']=="careerBazzar":
    			return HttpResponseRedirect('/careerbazzar/siteadmin/admin_panel/')
        	return HttpResponseRedirect("/careerbazzar/home/")
        	
        else:
        	return HttpResponseRedirect("/careerbazzar/invalid/")

    except:
    	return HttpResponseRedirect("/careerbazzar/invalid/")


def logout(request):
	if 'name' in request.session:
		auth.logout(request)
		return HttpResponseRedirect("/careerbazzar/home/")

	return HttpResponseRedirect('/careerbazzar/login/')


def invalid_login(request):
    if 'name' in request.session:
        return HttpResponseRedirect("/careerbazzar/home/")

    return render_to_response("invalid_login.html", context_instance=RequestContext(request))


def user_signup(request):

	if 'name' in request.session:
		address = "/careerbazzar/home/"
		return HttpResponseRedirect(address)

	form = captchaForm()
	
	if request.method == 'POST':
		form = captchaForm(request.POST)
		
		fname = request.POST.get("fname", "")
		lname = request.POST.get("lname", "")
		uname = request.POST.get("username", "")
		email = request.POST.get("email", "")
		passwd1 = request.POST.get("password1", "")
		passwd2 = request.POST.get("password2", "")
		old_user = User.objects.filter(email=email)

		if form.is_valid():
			if len(passwd1)<6:
				passwd_length_alert=True
			elif passwd1 != passwd2:
				passwd_alert = True


			else:
				if old_user:
					email_alert = True
			
				else:
					try:
						new_user = User(username=uname,email=email,password=make_password(passwd1))
						new_user.save()
						new_profile = userProfile(firstName=fname,lastName=lname,userName=uname,email=email)
						new_profile.save()
						success = True
					except:
						username_alert = True

	return render_to_response("user_signup.html",locals(), context_instance=RequestContext(request))


def employer_signup(request):

	if 'name' in request.session:
		address = "/careerbazzar/home/"
		return HttpResponseRedirect(address)
	
	form = captchaForm()

	if request.method == 'POST':
		form = captchaForm(request.POST)
		
		cname = request.POST.get("cname", "")
		uname = cname.replace(" ","-")
		email = request.POST.get("email", "")
		addr = request.POST.get("address", "")
		phone = request.POST.get("phone", "")
		type2 = request.POST.get("business", "")
		passwd1 = request.POST.get("password1", "")
		passwd2 = request.POST.get("password2", "")
		old_user = User.objects.filter(email=email)

		if form.is_valid():
			if passwd1 != passwd2:
				passwd_alert = True

			else:
				if old_user:
					email_alert = True
			
				else:
					try:
						new_user = User(username=uname,email=email,password=make_password(passwd1))
						new_user.save()
						new_profile = employerProfile(companyName=cname,cUserName=uname,companyEmail=email,companyAddress=addr,contactNo=phone,companyCategory=type2)
						new_profile.save()
						success = True
					except:
						username_alert = True

	category = jobType.objects.values_list('typeName',flat=True)

	return render_to_response("employer_signup.html",locals(), context_instance=RequestContext(request))